package ru.t1.shevyreva.tm;

import ru.t1.shevyreva.tm.component.Bootstrap;
import ru.t1.shevyreva.tm.constant.ApplicationConst;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.model.Command;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.service.CommandService;
import ru.t1.shevyreva.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
