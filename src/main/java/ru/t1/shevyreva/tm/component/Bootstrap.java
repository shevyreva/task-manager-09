package ru.t1.shevyreva.tm.component;

import ru.t1.shevyreva.tm.api.ICommandController;
import ru.t1.shevyreva.tm.api.ICommandRepository;
import ru.t1.shevyreva.tm.api.ICommandService;
import ru.t1.shevyreva.tm.constant.ArgumentConst;
import ru.t1.shevyreva.tm.constant.CommandConst;
import ru.t1.shevyreva.tm.controller.CommandController;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(final String[] args) {
        processArguments(args);
        System.out.println("**Welcome to Task Manager**");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        exit();
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command.toLowerCase()) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.COMMANDS:
                commandController.showCommand();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArgument();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommand();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArgument();
                break;
            default:
                commandController.showArgumentError();
                break;
        }
    }

    public void exit() {
        System.exit(0);
    }

}
