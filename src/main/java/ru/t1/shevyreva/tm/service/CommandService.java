package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.ICommandRepository;
import ru.t1.shevyreva.tm.api.ICommandService;
import ru.t1.shevyreva.tm.model.Command;
import ru.t1.shevyreva.tm.repository.CommandRepository;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
